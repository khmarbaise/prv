package prv.inv.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;


@SpringBootApplication
public class BizOnApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(BizOnApplication.class, args);
    }
}
