package prv.inv.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import prv.inv.api.repository.ZRepository;

@Service
public class A {

    Z delegate;

    @Autowired
    A(Z delegate){
        this.delegate = delegate;
    }


    @Transactional //if you comment @Transactional the test pass
    public Integer testValue(long id, String v) {
        return delegate.updateValue(id,v).getValue();
    }
}
