package prv.inv.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import prv.inv.api.entity.ZEntity;
import prv.inv.api.repository.ZRepository;

import java.util.Optional;

@Service
public class Z {

    ZRepository zRepository;

    @Autowired
    Z(ZRepository zRepository){
        this.zRepository = zRepository;
    }

    public ZEntity updateValue(Long id, String v) {
        ZEntity zEntity = zRepository.findById(id).orElseThrow(RuntimeException::new);

        Integer value = Integer.parseInt(v);
        zEntity.setValue(value);

        return zRepository.save(zEntity);
    }
}
