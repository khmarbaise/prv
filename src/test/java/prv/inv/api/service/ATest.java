package prv.inv.api.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.junit.Assert.*;

@Configuration
@ComponentScan(basePackages = "prv.inv.api")
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class ATest {

    @Autowired
    A a;

    @Test(expected = NumberFormatException.class)
    public void _1shouldThrowaNumberFormatException(){
        a.testValue(1,"101a");
    }

    @Test
    public void _2shouldThrowaNumberFormatException(){
        catchException(a).testValue(1,"101a");
        assertTrue(caughtException() instanceof NumberFormatException);
    }
}